﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FEG_calculator
{
    public partial class Calculator : Form
    {
        public Calculator()
        {
            InitializeComponent();
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            txtBoxDisplay.Text = txtBoxDisplay.Text + ((Button)sender).Text;
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            txtBoxDisplay.Text = txtBoxDisplay.Text + "1";
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            txtBoxDisplay.Text = txtBoxDisplay.Text + "2";
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            txtBoxDisplay.Text = txtBoxDisplay.Text + "3";
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            txtBoxDisplay.Text = txtBoxDisplay.Text + "4";
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            txtBoxDisplay.Text = txtBoxDisplay.Text + "5";
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            txtBoxDisplay.Text = txtBoxDisplay.Text + "6";
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            txtBoxDisplay.Text = txtBoxDisplay.Text + "7";
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            txtBoxDisplay.Text = txtBoxDisplay.Text + "8";
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            txtBoxDisplay.Text = txtBoxDisplay.Text + "9";
        }

        private void btnEq_Click(object sender, EventArgs e)
        {
            //string[] numbers = txtBoxDisplay.Text.Split('+', '-', '*', '/');
            if (txtBoxDisplay.Text != string.Empty)
            {
                try
                {
                    double result = Convert.ToDouble(new DataTable().Compute(txtBoxDisplay.Text, null));
                    txtBoxDisplay.Text = result.ToString();
                }
                catch
                {
                    txtBoxDisplay.Text = "Cannot compute";
                }

            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtBoxDisplay.Text.Length==0)
            {
                txtBoxDisplay.Text = txtBoxDisplay.Text + "0+";
            }
            else if (txtBoxDisplay.Text.Last() == '-' || txtBoxDisplay.Text.Last() == '+' || txtBoxDisplay.Text.Last() == '/' || txtBoxDisplay.Text.Last() == '*')
            {
                string noLast = txtBoxDisplay.Text.Remove(txtBoxDisplay.Text.Length - 1, 1);
                txtBoxDisplay.Text = noLast + "+";
            }
            else
            {
                txtBoxDisplay.Text = txtBoxDisplay.Text + "+";
            }
        }

        private void btnSub_Click(object sender, EventArgs e)
        {
            if (txtBoxDisplay.Text.Last() == '-' || txtBoxDisplay.Text.Last() == '+' || txtBoxDisplay.Text.Last() == '/' || txtBoxDisplay.Text.Last() == '*')
            {
                string noLast = txtBoxDisplay.Text.Remove(txtBoxDisplay.Text.Length - 1, 1);
                txtBoxDisplay.Text = noLast + "-";
            }
            else
            {
                txtBoxDisplay.Text = txtBoxDisplay.Text + "-";
            }
        }

        private void btnMult_Click(object sender, EventArgs e)
        {
            if (txtBoxDisplay.Text.Length == 0)
            {
                txtBoxDisplay.Text = txtBoxDisplay.Text + "0*";
            }
            else if (txtBoxDisplay.Text.Last() == '-' || txtBoxDisplay.Text.Last() == '+' || txtBoxDisplay.Text.Last() == '/' || txtBoxDisplay.Text.Last() == '*')
            {
                string noLast = txtBoxDisplay.Text.Remove(txtBoxDisplay.Text.Length - 1, 1);
                txtBoxDisplay.Text = noLast + "*";
            }
            else
            {
                txtBoxDisplay.Text = txtBoxDisplay.Text + "*";
            }
        }

        private void btnDiv_Click(object sender, EventArgs e)
        {
            if (txtBoxDisplay.Text.Length == 0)
            {
                txtBoxDisplay.Text = txtBoxDisplay.Text + "0/";
            }
            else if (txtBoxDisplay.Text.Last() == '-' || txtBoxDisplay.Text.Last() == '+' || txtBoxDisplay.Text.Last() == '/' || txtBoxDisplay.Text.Last() == '*')
            {
                string noLast = txtBoxDisplay.Text.Remove(txtBoxDisplay.Text.Length - 1, 1);
                txtBoxDisplay.Text = noLast + "/";
            }
            else
            {
                txtBoxDisplay.Text = txtBoxDisplay.Text + "/";
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            txtBoxDisplay.Text = string.Empty;
        }

        private void btnFullSt_Click(object sender, EventArgs e)
        {
            txtBoxDisplay.Text = txtBoxDisplay.Text + ".";
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (txtBoxDisplay.Text.Length > 0)
            {
                txtBoxDisplay.Text = txtBoxDisplay.Text.Remove(txtBoxDisplay.Text.Length - 1, 1);
            }
            if (txtBoxDisplay.Text == "")
            {
            
            }


        }
    }
}
